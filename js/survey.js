$(document).ready(function () {
	var landingpage = $('#landingpage');
	var registerpage = $('#registerpage');
	var surveypage = $('#surveypage');

	$('#gotoregister').click(function () {
		landingpage.fadeOut();
		registerpage.fadeIn();
	})
	$('#gotosurvey').click(function () {
		landingpage.fadeOut();
		surveypage.fadeIn();
	})
	//jQuery time
	var current_fs, next_fs, previous_fs; //fieldsets
	var left, opacity, scale; //fieldset properties which we will animate
	var animating; //flag to prevent quick multi-click glitches
	$(".next").click(function () {
		checked = $("#subForm-survey input[type=checkbox]:checked").length;
		var email = $("#subForm-survey input[type='email']").val();
		console.log(email);
		if(!email) {
			$('.error-survey').fadeIn();	
		  return false;
		}

		if (animating) return false;
		animating = true;

		
		current_fs = $(this).parent().parent();
		next_fs = $(this).parent().parent().next();

		//activate next step on progressbar using the index of next_fs
		var val = $('.progresscounter').text();
		intval = parseInt(val);
		 $('.progresscounter').html(intval+1);
		 $('#progresscircle').attr('src', 'img/progress'+(intval+1)+'.svg');
		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({
			opacity: 0
		}, {
			step: function (now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50) + "%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
					'transform': 'scale(' + scale + ')',
					'position': 'absolute'
				});
				next_fs.css({
					'left': left,
					'opacity': opacity
				});
			},
			duration: 800,
			complete: function () {
				current_fs.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
		$('.error-survey').hide();	
	});

	$(".previous").click(function () {
		if (animating) return false;
		animating = true;

		current_fs = $(this).parent().parent();
		previous_fs = $(this).parent().parent().prev();

		//de-activate current step on progressbar
		var val = $('.progresscounter').text();
		intval = parseInt(val);
		 $('.progresscounter').html(intval-1);
		$('#progresscircle').attr('src', 'img/progress'+(intval-1)+'.svg');
		//show the previous fieldset
		previous_fs.show();
		//hide the current fieldset with style
		current_fs.animate({
			opacity: 0
		}, {
			step: function (now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale previous_fs from 80% to 100%
				scale = 0.8 + (1 - now) * 0.2;
				//2. take current_fs to the right(50%) - from 0%
				left = ((1 - now) * 50) + "%";
				//3. increase opacity of previous_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
					'left': left
				});
				previous_fs.css({
					'transform': 'scale(' + scale + ')',
					'opacity': opacity
				});
			},
			duration: 800,
			complete: function () {
				current_fs.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	});

	$(".submit-survey").click(function (e) {
		$("#survey-button").hide();
		$("#loader-survey").show();
		e.preventDefault();
		$.ajax({
			url: 'https://oreo2.itracmediav4.com/survey?uuid=utdp3wr8',
			type: 'post',
			crossDomain: true,
			data: $('#subForm-survey').serialize(),
			success: function () {
				$("#loader-survey").hide();
				$('#surveypage').fadeOut();
				$('#thankyou-survey').fadeIn();
				$('#subForm-survey').trigger('reset');
				var sec = 7
				var timer = setInterval(function () {
					$('#hideMsg span').text(sec--);
					if (sec == -1) {
						$('#hideMsg').fadeOut('fast');
						clearInterval(timer);
						$('#thankyou-survey').fadeOut(function () {}, function () {
							window.location.href = "https://newseaton.com/brookfield-survey/";
						});
					}
				}, 1000);
			}
		});
	})

	$(".submit-register").click(function (e) {
		e.preventDefault();
		var empty = false;
		$('#subForm-register input').each(function () {
			if ($(this).val() == '') {
				empty = true;
			}
		});
		if (empty) {
			$('.error-register').show();
			return false;
		} else {
			$("#register-button").hide();
			$("#loader-register").show();

			$.ajax({
				url: 'https://beaches.itracmediav4.com/post?uuid=10b9b1e7-9ac5-43d0-a47d-f4be450dfedc',
				type: 'post',
				crossDomain: true,
				data: $('#subForm-register').serialize(),
				success: function () {
					$('.error-register').hide();
					$("#loader-register").hide();
					$('#registerpage').fadeOut();
					$('#thankyou-register').fadeIn();
					$('#subForm-register').trigger('reset');
					var sec = 7
					var timer = setInterval(function () {
						$('#hideMsg span').text(sec--);
						if (sec == -1) {
							$('#hideMsg').fadeOut('fast');
							clearInterval(timer);
							$('#thankyou-register').fadeOut(function () {}, function () {
								window.location.href = "https://newseaton.com/brookfield-survey/";
							});
						}
					}, 1000);
				}
			});

		}
	})

})